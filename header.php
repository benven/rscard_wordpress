<!DOCTYPE html>
<html lang="en" class=" theme-color-07cb79 theme-skin-light">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>RScard</title>
    <?php wp_head() ?>

	<!-- Favicon -->
	<link rel="shortcut icon" type="image/ico" href="img/favicon.png"/>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Modernizer for detect what features the user’s browser has to offer -->
</head>

<body class="loading header-has-img">

    <div class="mobile-nav">
        <button class="btn-mobile mobile-nav-close"><i class="rsicon rsicon-close"></i></button>
		
        <div class="mobile-nav-inner">
            <nav id="mobile-nav" class="nav">
				<ul class="clearfix">
					<li><a href="index.html#about">About</a></li>
					<li><a href="index.html#skills">Skills</a></li>
					<li><a href="index.html#portfolio">Portfolio</a> </li>
					<li><a href="index.html#experience">Experience</a></li>
					<li><a href="index.html#references">References</a></li>
					<li>
						<a href="category.html">Blog</a>
						<ul>
							<li><a href="single-image.html">Image Post</a></li>
							<li><a href="single-slider.html">Slider Post</a></li>
							<li><a href="single-video.html">Video Post</a></li>
							<li><a href="single-audio.html">Audio Post</a></li>
							<li><a href="single-vimeo.html">Vimeo Post</a></li>
							<li><a href="single-youtube.html">Youtube Post</a></li>
							<li><a href="single-dailymotion.html">Dailymotion Post</a></li>
							<li><a href="single.html">Without Media Post</a></li>
							<li><a href="typography.html">Typography Page</a></li>
							<li><a href="icons.html">Icons Page</a></li>
							<li><a href="404.html">404 Page</a></li>
						</ul>
					</li>
					<li><a href="index.html#calendar">Calendar <span></span></a></li>
					<li><a href="index.html#contact">Contact <span></span></a></li>
				</ul>
			</nav>
        </div>
    </div><!-- .mobile-nav -->

   <?php get_sidebar(); ?>

    <div class="wrapper">
        <header class="header">
            <div class="head-bg" style="background-image: url('img/uploads/rs-cover.jpg')"></div>

            <div class="head-bar">
                <div class="head-bar-inner">
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">                            
                            <a class="logo" href="index.html"><span>RS</span>card</a>
							<!-- <a class="head-logo" href=""><img src="img/rs-logo.png" alt="RScard"/></a> -->
                        </div>

                        <div class="col-sm-9 col-xs-6">
                            <div class="nav-wrap">
                                <nav id="nav" class="nav">
									<ul class="clearfix">
										<li><a href="index.html#about">About</a></li>
										<li><a href="index.html#skills">Skills</a></li>
										<li><a href="index.html#portfolio">Portfolio</a> </li>
										<li><a href="index.html#experience">Experience</a></li>
										<li><a href="index.html#references">References</a></li>
										<li>
											<a href="category.html">Blog</a>
											<ul>
												<li><a href="single-image.html">Image Post</a></li>
												<li><a href="single-slider.html">Slider Post</a></li>
												<li><a href="single-video.html">Video Post</a></li>
												<li><a href="single-audio.html">Audio Post</a></li>
												<li><a href="single-vimeo.html">Vimeo Post</a></li>
												<li><a href="single-youtube.html">Youtube Post</a></li>
												<li><a href="single-dailymotion.html">Dailymotion Post</a></li>
												<li><a href="single.html">Without Media Post</a></li>
												<li><a href="typography.html">Typography Page</a></li>
												<li><a href="icons.html">Icons Page</a></li>
												<li><a href="404.html">404 Page</a></li>
											</ul>
										</li>
						<!-- <link rel="shortcut icon" type="image/ico" href="img/favicon.png"/> -->
                        <li><a href="index.html#calendar">Calendar <span></span></a></li>
										<li><a href="index.html#contact">Contact <span></span></a></li>
									</ul>
								</nav>

                                <button class="btn-mobile btn-mobile-nav">Menu</button>
                                <button class="btn-sidebar btn-sidebar-open"><i class="rsicon rsicon-menu"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header><!-- .header -->
        <div class="content">
            <div class="container">