<?php
// Add scripts and stylesheets
add_action( 'wp_enqueue_scripts', 'custom_scripts' );
add_action('after_setup_theme', 'montheme_supports');
add_theme_support( 'post-thumbnails' );

function montheme_supports () {
    add_theme_support('title-tag');
}
function custom_scripts() {

    //Google Fonts
    wp_enqueue_style( 'font-fredoka', 'https://fonts.googleapis.com/css?family=Fredoka+One');
    wp_enqueue_style( 'font-opensans', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic');

   //Icon Fonts -->
    wp_enqueue_style( 'map-icons', get_template_directory_uri() . '/fonts/map-icons/css/map-icons.min.css' );
    wp_enqueue_style( 'icomoon', get_template_directory_uri() . '/fonts/icomoon/style.css' );

    //functions
    wp_enqueue_style( 'bx-slider', get_template_directory_uri() . '/js/plugins/jquery.bxslider/jquery.bxslider.css' );
    wp_enqueue_style( 'custom-scroll', get_template_directory_uri() . '/js/plugins/jquery.customscroll/jquery.mCustomScrollbar.min.css' );
    wp_enqueue_style( 'mediaelement', get_template_directory_uri() . '/js/plugins/jquery.mediaelement/mediaelementplayer.min.css' );
    wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/js/plugins/jquery.fancybox/jquery.fancybox.css' );
    wp_enqueue_style( 'owlcarousel', get_template_directory_uri() . '/js/plugins/jquery.owlcarousel/owl.carousel.css' );

    wp_enqueue_style( 'owlcarousel-theme', get_template_directory_uri() . '/js/plugins/jquery.owlcarousel/owl.theme.css');
    wp_enqueue_style( 'optionpanel', get_template_directory_uri() . '/js/plugins/jquery.optionpanel/option-panel.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style( 'theme-color', get_template_directory_uri() . '/colors/theme-color.css');
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/libs/modernizr.js');

    //Footer
    wp_enqueue_script( 'ajax-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js', [], false, true);
    wp_enqueue_script( 'maps', 'https://maps.googleapis.com/maps/api/js', [], false, true);
    wp_enqueue_script( 'site', get_template_directory_uri() . '/js/site.js', [], false, true);
}

