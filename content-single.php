<div class="grid-item">
    <article class="post-box animate-up">
        <div class="post-media">
            <div class="post-image">
                <a href="single.html"><img src="img/uploads/thumb-449x286-1.jpg" alt=""> </a>
            </div>
        </div>

        <div class="post-data">
            <time class="post-datetime" datetime="2015-03-13T07:44:01+00:00">
                <?php the_date(); ?>
            </time>

            <div class="post-tag">
                <a href="category.html"><?php the_tags(); ?></a>
            </div>

            <h3 class="post-title">
                <a href="single-image.html"><?php the_title(); ?></a>
            </h3>

            <?php
                if ( has_post_thumbnail() ) {
                    echo '<div class="mb-4">';
                    the_post_thumbnail('medium');
                    echo '</div>';
                }
            ?>
            <?php the_excerpt() ?>

            <div class="post-info">
                <a href="category.html"><i class="rsicon rsicon-user"></i>by <?php the_author(); ?></a>
                <a href="category.html"><i class="rsicon rsicon-comments"></i>56</a>
            </div>
        </div>
    </article>
</div>
